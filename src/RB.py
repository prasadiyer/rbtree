class RB:
	def __init__(self, arg0):
		self.val=arg0
		self.parent=None
		self.left=None
		self.right=None
		self.red=True
        def gg(self):
                if self.parent is None:
                        return None
                if self.parent.parent is None:
                        return None
                return self.parent.parent
        def uncle(self):
                if self.gg() is None:
                        return
                if self.gg().left.val == self.parent.left.val:
                        return self.gg().right
                return self.gg().left
        def isleft(self):
                if self.parent is None:
                        return None
                if self.parent.left.val == self.val:
                        return True
                else:
                        return False
        def change(self, P=None, L=None, R = None):
                if P is not None:
                        self.parent = P
                if L is not None:
                        L.parent = self
                        self.left = L
                if R is not None:
                        R.parent = self
                        self.right = R
                
                        
                
class TreeRB:
	def __init__(self):
		self.t=None
	def add(self, arg0):
		if self.t is None:
			self.t = RB(arg0)
			return
		self.__add(self.t, arg0)
	def __add(self, T, arg0):
		if T.val > arg0:
			if T.left is None:
				K=RB(arg0)
				T.left =K
				K.parent = T
                                self.rebalance(K);
                                return
			else:
				self.__add(T.left, arg0)
		elif T.val < arg0:
                        if T.right is None:
                                K=RB(arg0)
                                T.right = K
                                K.parent =T
                                self.rebalance(K)
                                return
                        else:
                                self.__add(T.right, arg0)
        def rebalance(self, arg0):
                if arg0.parent is None:
                        arg0.red = False
                        return
                if arg0.parent.red is False:
                        return
                if arg0.uncle() is not None and arg0.uncle().red is True:
                        arg0.parent.red = False
                        arg0.uncle().red = False
                        arg0.gg().red = True
                        arg0.rebalance(arg0.gg())
                        return
                else :
                        self.pivot(arg0)
        def pivot(self, arg0):
                if arg0.gg() is None:
                        return
                if arg0.isleft() and arg0.parent.isleft():
                        return pivotRight(arg0.parent)
                if arg0.isleft() and arg0.parent.isleft() is False:
                        return pivotRight(arg0, True)
                if arg0.isleft() is False and arg0.parent.isleft() is False:
                        return pivotLeft(arg0.parent)
                if arg0.isleft() is False and arg0.parent.isleft() is True:
                        return pivotLeft(arg0, True)
        def pivotLeft(self, arg0, arg1=False):
                if arg1 is True:
                        self.pivotRight(arg0)
                        self.pivotLeft(arg0)
                else:
                        P = arg0.parent
                        GG = arg0.gg()
                        P.change(arg0, P.left, arg0.left)
                        GG.change(None, None, arg0)
                        arg0.change(GG, P, arg0.right)
        def pivotRight(self, arg0, arg1=False):
                if arg1 is True:
                        self.pivotLeft(arg0)
                        self.pivotRight(arg0)
                else:
                        P = arg0.parent
                        GG = arg0.gg()
                        P.change(arg0, arg0.right, P.right)
                        GG.change(None, arg0, None)
                        arg0.change(GG, arg0.left, P)
                        
                        
                        
                        
                
                
                                
	def printme(self):
                self.__printme(self.t)
        def __printme(self, arg):
                if arg is None:
                        return
                self.__printme(arg.left)
                print arg.val
                self.__printme(arg.right)
                
	                                


rb=TreeRB()
rb.add(10)
rb.add(7)
rb.add(19)
rb.add(3)
rb.printme()







