

class CNode:
    def __init__(self, arg0):
        self.value=arg0
        self.red=True
        self.left=None
        self.right=None
        self.parent=None

    def _black_(self):
        self.red=False

    def _red_(self):
        self.red=True

    def _color_(self):
        if self.red==True:
            return 'RED'
        return 'BLACK'

    def _is_red(self):
        return self.red



    def __eq__(self, other):
        if other is None:
            return False
        if (isinstance(other, CNode)):
            return self.value == other.value
        else:
            return False




class NodeHandler:
    def __init__(self):
        self.node=None

    def add(self, arg0, arg1=None):
        root=self.node
        if arg1 is not None:
            root=arg1
        if root is None:
            self.node=CNode(arg0)
            self.node._black_()
            self._rebalance(self.node)
            return
        if root.value == arg0:
            print "Duplicate found"
            return
        elif root.value > arg0:
            if root.left is not None:
                self.add(arg0, root.left)
            else:
                tmp=CNode(arg0)
                root.left=tmp
                tmp.parent=root
                self._rebalance(tmp)
                return
        elif root.value < arg0:
            if root.right is not None:
                self.add(arg0, root.right)
            else:
                tmp=CNode(arg0)
                root.right=tmp
                tmp.parent=root
                self._rebalance(tmp)
                return


    def _rebalance(self, arg0):
        print arg0.value

        # Case 1 : if it is root node
        if arg0.parent is None:
            print 'parent node coloring black'
            arg0._black_()
            return
        """Case 2 : if the parent node is black
            no need to do anything as the black height is not changed"""
        if arg0.parent._is_red()==False:

            print "No need to do anything %s" % arg0.parent.value
            return

        """Case 3 : We know that the parent is red
            We check if the the node doesn't have the uncle we just pivot
        """
        if self.get_uncle(arg0) is None:
            if self._left_parent(arg0) == True:
                print "pivoting left"
                pass
            else:
                self.pivot_right(arg0)
        else:
            if self.get_uncle(arg0)._is_red():
                print 'uncle is red'
                self.get_uncle(arg0)._black_()
                self.get_parent(arg0)._black_()
                self.get_grand_parent(arg0)._red_()
                print "parent called %s" % self.get_grand_parent(arg0).value
                self._rebalance(self.get_grand_parent(arg0))
            else:
                print 'my uncle is black'



    def pivot_right(self, arg0):
        changeroot=False
        ggp=None
        if arg0.parent.value > arg0.value:
            pass
        else:
            """ Performing the rotation
            1. get the grand parent, parent and great grand parent
            2. make the grand parent the child of the parent
            3. make the great grand parent parent of parent
            """
            if self.get_grand_parent(arg0) is not None and self.get_grand_parent(arg0).parent is None:
                ggp=self.get_grand_parent(arg0).parent
                changeroot=True

            gp=self.get_grand_parent(arg0)
            p=self.get_parent(arg0)
            p.left=gp
            p.parent=ggp

            if gp is not None:
                gp.left=None
                gp.right=None
            if changeroot:
                self.node=p
                self.node._black_()
                if gp is not None:
                    gp._red_()
            else:
                print 'what to do'


            print 'pivoting right'
            pass


    def get_parent(self, arg0):
        return arg0.parent


    def get_parent_value(self, arg0):
        return arg0.parent.value

    def get_grand_parent(self, arg0):
        if arg0.parent is None:
            return None
        return arg0.parent.parent

    def get_uncle(self, arg0):
        if self.get_grand_parent(arg0) is not None:
            if (self.get_grand_parent(arg0).left == self.get_parent(arg0)) == False:
                return self.get_grand_parent(arg0).left
            elif (self.get_grand_parent(arg0).right == self.get_parent(arg0)) == False:
                return self.get_grand_parent(arg0).right
        print "No uncle found"
        return None

    def _left_parent(self, arg0):
        _n=self.get_grand_parent(arg0)
        if _n is None:
            return False
        if _n.left is not None and _n.left== arg0.parent:
            return True
        else:
            return False

    def _print_root(self):
        print "%s root" % self.node.value


    def printtree(self, arg0=None):
        root=arg0
        if root is None:
            root=self.node
        print "%s [%s] " % (root.value, root._color_())
        if root.left is not None:
            self.printtree(root.left)
        if root.right is not None:
            self.printtree(root.right)



nodeHandler=NodeHandler()
nodeHandler.add(10)
nodeHandler.add(20)
nodeHandler.add(30)
nodeHandler.add(40)
# nodeHandler.add(50)
# nodeHandler.add(60)
# nodeHandler.add(70)
nodeHandler.printtree()
print "----------------"
nodeHandler._print_root()
print "----------------"
nodeHandler.printtree()






